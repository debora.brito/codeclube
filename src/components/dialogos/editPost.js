import Grid from "@material-ui/core/Grid";
import React, { Component } from "react";
import api from "../../services/api";
import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import FormHelperText from "@material-ui/core/FormHelperText";
import TextField from "@material-ui/core/TextField";

class EditPost extends Component {
  state = {
    open: false,
    titulo: "",
    descricao: "",
    conteudo: "",
    error: "",
    total: "",
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    const id = this.props.match.params.id
    api.get(`/posts/${id}`).then(
      (res) => {
        this.setState({
          titulo: res.data.post.titulo,
          descricao: res.data.post.descricao,
          conteudo: res.data.post.conteudo 
        });
      },
      (err) => {
        console.log(err);
      }
    );
  }

  handleUpdate = async (e) => {
    e.preventDefault();
    const { titulo, descricao, conteudo} = this.state;
    const id = this.props.match.params.id
    if (!titulo || !descricao || !conteudo) {
      this.setState({ error: "Preencha todos os dados para se cadastrar" });
    } else {
      try {
        await api.put(`/posts/${id}`, { titulo, descricao, conteudo });
        window.location.reload()
      } catch (err) {
        console.log(err);
        this.setState({ error: "Ocorreu um erro ao registrar sua conta. T.T" });
      }
    }
  }

  render() {
    return (
      <Grid item>
        <IconButton
          variant="outlined"
          color="primary"
          onClick={this.handleClickOpen}
        >
          <EditIcon />
        </IconButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <form
            method="PUT"
            onSubmit={this.handleUpdate}
            noValidate
            autoComplete="off"
          >
            <DialogTitle id="form-dialog-title">Edite sua pergunta</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Para editar sua pergunta preencha os campos abaixo.
              </DialogContentText>
              <FormHelperText style={{ color: "red" }}>
                {this.state.error && <span>{this.state.error}</span>}
              </FormHelperText>
              <TextField
                autoFocus
                margin="dense"
                id="standard-textarea"
                label="Titulo"
                defaultValue={this.state.titulo}
                fullWidth
                variant="filled"
                multiline
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => this.setState({ titulo: e.target.value })}
              />
              <TextField
                autoFocus
                margin="dense"
                id="standard-textarea"
                label="Descrição"
                defaultValue={this.state.descricao}
                multiline
                fullWidth
                variant="filled"
                rows={3}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => this.setState({ descricao: e.target.value })}
              />
              <TextField
                autoFocus
                margin="dense"
                id="standard-textarea"
                label="Conteúdo"
                defaultValue={this.state.conteudo}
                multiline
                fullWidth
                variant="filled"
                rows={10}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => this.setState({ conteudo: e.target.value })}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Cancelar
              </Button>
              <Button type="submit" color="primary">
                Enviar
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </Grid>
    );
  }
}

export default withRouter(EditPost);

