import React, { Component } from "react";
import CursoAdmin from "../Admin/cursos/index";
import CursoAluno from "../Aluno/cursos/index";
import Lista from "../Admin/cursos/lista";
import api from "../../services/api";
import { withRouter } from "react-router-dom";

class Cursos extends Component {
	state = {
		role: "",
	};

	async componentDidMount() {
		const response = await api.get("/roles/get-role");
        this.setState({ role: response.data.Role.role_id });
	}

	render() {
		return (
			<div style={{ marginTop: "110px" }}>
				{this.state.role === 1 ? (
					<div>
						{" "}
						<CursoAdmin /> <Lista />{" "}
					</div>
				) : (
					<CursoAluno />
				)}
			</div>
		);
	}
}

export default withRouter(Cursos);
