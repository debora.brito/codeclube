import React, { useEffect, useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import api from "../../services/api";
import Pagination from "../Pagination/index";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from '@material-ui/core/styles';
import FavoriteIcon from "@material-ui/icons/Favorite";
import ModeCommentIcon from "@material-ui/icons/ModeComment";
import Footer from "../footer/footer";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid"

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    paddingRight: "16px",
    paddingLeft: "16px",
    marginLeft: "60px",
    maxWidth: "150ch",
    display: "inline-block",
    alignItems: "center",
  },
  inline: {
    display: "inline",
  },
  pagination:{
      borderTop: `1px solid ${theme.palette.divider}`,
      // marginTop: theme.spacing(6),
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3),
  },
  divider: {
    height: 1,
    backgroundColor: "#63a4ff",
    marginTop: "10px",
  },
  paper: {
    margin: `${theme.spacing(1)}px auto`,
    marginLeft: '100px',
    marginRight: '100px'
    // padding: theme.spacing(2),
  },
}));

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  "@global": {
    ".MuiTableContainer-root": {
      // width: "80%",
    },
  },
})(() => null);

export default function Lista() {
  const [posts, setPosts] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(10);
  const classes = useStyles();
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  useEffect(() => {
    const fetchPosts = async () => {
      const res = await api.get("/posts");
      setPosts(res.data.posts);
    };
    fetchPosts();
  }, []);


  return (
    <div>
      <GlobalCss />
      <Grid container >
      <TableContainer component={Paper} className={classes.paper}>
        <Table className={classes.table} aria-label="custom pagination table">
          <TableBody>
            {currentPosts.map((post) => (
              <TableRow key={post.id}>
                <TableCell component="th" scope="row">
                  <Link
                    to={`forum/post/${post.id}`}
                    style={{ color: "#3f51b5" }}
                  >
                    <Typography variant="h6" style={{ fontSize: "25px" }}>
                      {post.titulo}
                    </Typography>
                  </Link>
                  <Typography variant="subtitle2">{post.descricao}</Typography>
                  <Typography
                    style={{
                      color: "#63a4ff",
                      position: "relative",
                      top: "10px",
                    }}
                    variant="body2"
                  >
                    12 pessoas responderam
                    <ModeCommentIcon
                      fontSize="small"
                      style={{ position: "absolute" }}
                    />
                  </Typography>
                  <Typography
                    style={{
                      color: "#63a4ff",
                      position: "relative",
                      top: "10px",
                    }}
                    variant="body2"
                  >
                    12 pessoas seguindo
                    <FavoriteIcon
                      fontSize="small"
                      style={{ position: "absolute" }}
                    />
                  </Typography>
                </TableCell>
                <TableCell align="right">
                  <Typography color="primary" variant="caption">
                    {post.created_at.substr(8, 2) +
                      "/" +
                      post.created_at.substr(5, 2) +
                      "/" +
                      post.created_at.substr(0, 4)}
                  </Typography>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Grid item xs>
        <Pagination className={classes.pagination}
          postsPerPage={postsPerPage}
          totalPosts={posts.length}
          paginate={paginate}
        />
      </Grid>
      <Footer />
      </Grid>
      
    </div>
  );
}
