import React, { Component } from "react";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import { Divider, Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import api from "../../services/api";
import FormHelperText from "@material-ui/core/FormHelperText";
import Lista from "./index";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";

const ocorrencia = [
  {
    value: "abc",
    label: "Mais recente",
  },
  {
    value: "def",
    label: "Mais votado",
  },
  {
    value: "ghi",
    label: "Mais recomendado",
  },
];

const currencies = [
  {
    value: "USD",
    label: "Desplugadas",
  },
  {
    value: "EUR",
    label: "Scratch",
  },
  {
    value: "BTC",
    label: "Microbit",
  },
  {
    value: "JPY",
    label: "Todas",
  },
];

const filters = [
  {
    value: "OPQ",
    label: "Perguntas que estou seguindo",
  },
  {
    value: "IJK",
    label: "Perguntas que eu fiz",
  },
  {
    value: "LMN",
    label: "Perguntas sem respostas",
  },
];

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  "@global": {
    // You should target [class*="MuiButton-root"] instead if you nest themes.

    ".MuiTypography-body1": {
      // padding: "8px 12px",
      flexGrow: "1",
    },
    ".MuiDialogTitle-root": {
      color: "#000088",
    },
  },
})(() => null);

const useStyles = (theme) => ({
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
    width: 400,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  heroContent: {
    padding: theme.spacing(10, 0, 0, 0),
    flexGrow: "1",
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  divider: {
    height: 1,
    // width: '100%',
    backgroundColor: "#63a4ff",
    marginTop: "10px",
    marginBottom: "40px",
  },
});

class Forum extends Component {
  state = {
    currency: "EUR",
    ocorrencias: "abc",
    filter: "OPQ",
    open: false,
    titulo: "",
    descricao: "",
    conteudo: "",
    error: "",
    total: "",
  };

  async componentDidMount() {
    const response = await api.get("/posts");
    this.setState({ total: response.data.posts.length });
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  changeHandle = (event) => {
    this.setState({ ocorrencias: event.target.value });
  };

  handleChange = (event) => {
    this.setState({ currency: event.target.value });
  };

  handlFilters = (event) => {
    this.setState({ filter: event.target.value });
  };

  handlePost = async (e) => {
    e.preventDefault();
    const { titulo, descricao, conteudo, user } = this.state;
    if (!titulo || !descricao || !conteudo) {
      this.setState({ error: "Preencha todos os dados para se cadastrar" });
    } else {
      try {
        await api.post("/posts", { titulo, descricao, conteudo, user });
        window.location.reload("/forum");
      } catch (err) {
        console.log(err);
        this.setState({ error: "Ocorreu um erro ao registrar sua conta. T.T" });
      }
    }
  };
  

  render() {
    const { classes } = this.props;
    const { total } = this.state;
    return (
      <div>
        <GlobalCss />
        <Grid container spacing={1} className={classes.heroContent}>
          <Paper component="form" className={classes.root}>
            <IconButton
              color="primary"
              type="submit"
              className={classes.iconButton}
              aria-label="search"
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              className={classes.input}
              placeholder="Pesquise"
              inputProps={{ "aria-label": "procure por perguntas" }}
            />
            <Divider className={classes.divider} orientation="vertical" />
          </Paper>
          <TextField
            id="filled-select-ocorrencias"
            select
            label="Filtrar"
            value={this.state.filter}
            onChange={this.handlFilters}
            variant="outlined"
          >
            {filters.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            id="filled-select-ocorrencias"
            select
            label="Classificar"
            value={this.state.ocorrencias}
            onChange={this.changeHandle}
            variant="outlined"
          >
            {ocorrencia.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            id="filled-select-currency"
            select
            label="Categorias"
            value={this.state.currency}
            onChange={this.handleChange}
            variant="outlined"
          >
            {currencies.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          {/* </Grid> */}
        </Grid>
        <Grid container>
          {/* <Grid item xs> */}
          <Typography
            style={{
              fontWeight: "500",
              marginTop: "40px",
              position: "relative",
              left: "70px",
            }}
          >
            {total} perguntas neste curso
          </Typography>
          {/* </Grid> */}
          {/* <Grid item> */}
          <Button
            variant="contained"
            color="primary"
            style={{
              marginRight: "100px",
              marginTop: "20px",
              position: "relative",
              left: "70px",
            }}
            onClick={this.handleClickOpen}
          >
            Nova Pergunta
          </Button>
          <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
          >
            <form
              method="POST"
              onSubmit={this.handlePost}
              noValidate
              autoComplete="off"
            >
              <DialogTitle id="form-dialog-title">Nova Pergunta</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  Para fazer uma nova pergunta preencha os campos abaixo.
                </DialogContentText>
                <FormHelperText style={{ color: "red" }}>
                  {this.state.error && <span>{this.state.error}</span>}
                </FormHelperText>
                <TextField
                  autoFocus
                  margin="dense"
                  id="standard-textarea"
                  label="Titulo"
                  placeholder="Digite aqui o título"
                  fullWidth
                  variant="filled"
                  multiline
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(e) => this.setState({ titulo: e.target.value })}
                />
                <TextField
                  autoFocus
                  margin="dense"
                  id="standard-textarea"
                  label="Descrição"
                  placeholder="Faça uma breve descrição do assunto"
                  multiline
                  fullWidth
                  variant="filled"
                  rows={3}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(e) => this.setState({ descricao: e.target.value })}
                />
                <TextField
                  autoFocus
                  margin="dense"
                  id="standard-textarea"
                  label="Conteúdo"
                  placeholder="Digite aqui o conteúdo da sua pergunta"
                  multiline
                  fullWidth
                  variant="filled"
                  rows={10}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={(e) => this.setState({ conteudo: e.target.value })}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                  Cancelar
                </Button>
                <Button type="submit" color="primary">
                  Enviar
                </Button>
              </DialogActions>
            </form>
          </Dialog>
          <Divider variant="inset" className={classes.divider} flexItem />
        </Grid>
        <Grid container spacing={2}>
          <Lista />
        </Grid>
      </div>
    );
  }
}

export default withStyles(useStyles)(Forum);
