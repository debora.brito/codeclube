import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import React, { useEffect, useState } from "react";
import api from "../../services/api";
import { useParams } from "react-router-dom";
import { Typography } from "@material-ui/core";
import Morty from "../../imagens/Morty.jpg";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Editar from "../dialogos/editPost";
import Footer from "../footer/footer";
import Comentar from "../Comments/index";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    position: "relative",
    paddingTop: "80px",
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    margin: "auto",
    maxWidth: "1000px",
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

export default function FullWidthGrid() {
  const classes = useStyles();
  const [post, setPost] = useState([]);
  const { id } = useParams();
  const [open, setOpen] = React.useState(false);
  const [error, setError] = useState("");
  const [sucess, setSucess] = useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    const fetchPosts = async () => {
      const res = await api.get(`/posts/${id}`);
      setPost(res.data.post);
      setSucess(res.data.sucesso);
    };
    fetchPosts();
  }, []);

  const deletePost = (id) => {
    // debugger;
    api
      .delete("/posts/" + id)

      .then((result) => {
        window.location.reload("/forum");
      });
  };

  console.log(sucess);
  if (sucess) {
    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container wrap="nowrap" spacing={2}>
            <Grid item>
              <Avatar alt="Morty" src={Morty} />
            </Grid>
            <Grid item xs={12} sm container>
              <Grid item xs container direction="column" spacing={2}>
                <Grid item xs>
                  <Typography
                    align="left"
                    gutterBottom
                    variant="h5"
                    color="textPrimary"
                    style={{ fontWeight: "bold" }}
                  >
                    {post.titulo}
                  </Typography>
                  <Typography
                    align="justify"
                    variant="subtitle2"
                    gutterBottom
                    color="textPrimary"
                  >
                    {post.descricao}
                  </Typography>
                  <Typography
                    align="justify"
                    variant="body2"
                    color="textSecondary"
                  >
                    {post.conteudo}
                  </Typography>
                  <Grid
                    item
                    xs={12}
                    align="left"
                    style={{ position: "relative", top: "10px" }}
                  >
                    <Comentar />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Editar />
              <IconButton
                variant="outlined"
                color="secondary"
                onClick={handleClickOpen}
              >
                <DeleteIcon />
              </IconButton>
              <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">
                  {"Tem certeza que deseja excluir?"}
                </DialogTitle>
                <DialogActions>
                  <Button onClick={handleClose} color="primary">
                    Cancelar
                  </Button>
                  <Button
                    onClick={() => {
                      deletePost(post.id);
                    }}
                    color="secondary"
                    autoFocus
                  >
                    Excluir
                  </Button>
                </DialogActions>
              </Dialog>
            </Grid>
          </Grid>
        </Paper>
        <Footer />
      </div>
    );
  } else {
    return <div style={{ paddingTop: "100px" }}>404notfound</div>;
  }
}
