import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Footer from "../../footer/footer";
import api from "../../../services/api";
import { useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
	"@global": {
		ul: {
			margin: 0,
			padding: 0,
			listStyle: "none",
		},
	},
	heroContent: {
		padding: theme.spacing(8, 0, 6),
	},
	cardHeader: {
		backgroundColor: "#1976d2",
		color: "white",
	},
}));

export default function Cursos() {
	const classes = useStyles();
	const [cursos, setCursos] = useState([]);
	const { id } = useParams();
	const [inscricao, setInscricao] = useState("");
	const [sucess, setSucess] = useState(false);

	useEffect(() => {
		const fetchCourses = async () => {
			const res = await api.get("/cursos");
			setCursos(res.data.cursos);
		};
		fetchCourses();
	}, []);

	const inscricaoPost = (id) => {
		// debugger;
		api.get(`/cursos/${id}/inscricao`).then((result) => {
            setSucess(true);
            setInscricao(res.data.inscricao);
		});
	};

	// const inscrito = async () => {
	// 	const res = await api.get("/inscricao/cursos");
	// 	console.log(res.data.cursos[0]);
	// 	setInscricao(res.data.cursos[0]);
	// };

	return (
		<React.Fragment>
			<CssBaseline />
			<Container
				maxWidth="sm"
				component="main"
				className={classes.heroContent}
			>
				<Typography
					component="h1"
					variant="h2"
					align="center"
					color="textPrimary"
					gutterBottom
				>
					Cursos
				</Typography>
				<Typography
					variant="h6"
					align="center"
					color="textPrimary"
					component="p"
				>
					Todos os cursos que oferecemos a nossos alunos =)
				</Typography>
			</Container>
			<Container maxWidth="md" component="main">
				<Grid container spacing={5} alignItems="flex-end">
					{cursos.map((curso) => (
						<Grid item key={curso.id} xs={12} md={4}>
							<Card>
								<CardHeader
									title={curso.nome_curso}
									subheader={curso.subheader}
									titleTypographyProps={{ align: "center" }}
									subheaderTypographyProps={{
										align: "center",
									}}
									className={classes.cardHeader}
								/>
								<CardContent>
									<ul>
										<Typography
											component="li"
											variant="subtitle1"
											align="center"
											key={curso}
										>
											{curso.descricao}
										</Typography>
									</ul>
								</CardContent>
								<CardActions>
                                    {console.log(inscricao.curso_id)}
									{curso.curso_id === id ? (
									<Button
										fullWidth
										variant="outlined"
										color="primary"
										onClick={() => {
											inscricaoPost(curso.id);
										}}
									>
										Inscreva-se
									</Button>
                                    ) : (
										<Button
											fullWidth
											variant="outlined"
											color="primary"
										>
											Inscrito!
										</Button>
                                    )}
								</CardActions>
							</Card>
						</Grid>
					))}
				</Grid>
				<Footer />
			</Container>
		</React.Fragment>
	);
}
