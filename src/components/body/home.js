import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import "./styles.css";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Footer from "../footer/footer";
import { Link } from "react-router-dom";
import Code from "../../imagens/code.png";

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  grid: {
    backgroundColor: "#fffff",
  },
  heroContent: {
    backgroundColor: "#63a4ff17",
    padding: theme.spacing(11),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  container: {
    flexDirection: "row",
    flexWrap: "wrap",
    display: "flex",
  },
  MuiLink:{
    display: 'block',
    height: '100%',
    width: '100%',
    "&:hover": {
    background: 'rgba(99, 164, 255, 0.651)',
    transition: 'all 1s',
    },
    // -moz-transition: 'all 1s',
    // -webkit-transition: all 1s;
  },
}));

export default function Home() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container className={classes.container}>
            {/* End hero unit */}
            <Grid container spacing={3}>
              <Grid item xs className={classes.container}>
                <Typography
                  style={{ fontWeight: "bold", color: "#0000ba" }}
                  component="h1"
                  variant="h2"
                  color="textPrimary"
                  gutterBottom
                >
                  Bem Vindo!
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs={8} className={classes.container}>
              <Typography
                variant="h5"
                color="textSecondary"
                paragraph
                style={{ fontWeight: "400", color: "#1976d2", width: "550px", textAlign: "justify" }}
              >
                Bem vindo a plataforma do Code Clube, aqui é o começo de tudo,
                onde você irá encontrá as principais funcionalidades para
                iniciar sua jornada.
              </Typography>
            </Grid>
            <Grid item xs>
            <img
              style={{ maxWidth: "300px"}}
              src={Code}
              alt="robos code clube"
            />
            </Grid>
          </Container>
        </div>

        

        <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
            <Grid item xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://source.unsplash.com/random"
                  title="Image title"
                />
                <Link to="/cursos" className={classes.MuiLink}>
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      Cursos
                    </Typography>
                    <Typography>
                      Confira aqui os cursos disponíveis na plataforma
                    </Typography>
                  </CardContent>
                </Link>
              </Card>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://source.unsplash.com/random"
                  title="Image title"
                />
                <Link className={classes.MuiLink} to="/forum">
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      Fórum
                    </Typography>

                    <Typography>
                      Deposite e tire suas dúvidas sobre as atividades
                    </Typography>
                  </CardContent>
                </Link>
              </Card>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://source.unsplash.com/random"
                  title="Image title"
                />
                <Link to="/">
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      Classificação
                    </Typography>
                    <Typography>
                      Confira aqui sua classificação geral e o ranking de todos
                      os alunos
                    </Typography>
                  </CardContent>
                </Link>
              </Card>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://source.unsplash.com/random"
                  title="Image title"
                />
                <Link to="/">
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      Chat
                    </Typography>
                    <Typography>Converse com alunos e professores</Typography>
                  </CardContent>
                </Link>
              </Card>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://source.unsplash.com/random"
                  title="Image title"
                />
                <Link to="/">
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      Feedback
                    </Typography>
                    <Typography>
                      Reporte bugs e dê sua opinião sobre a plataforma
                    </Typography>
                  </CardContent>
                </Link>
              </Card>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.cardMedia}
                  image="https://source.unsplash.com/random"
                  title="Image title"
                />
                <Link to="/">
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      Desafios
                    </Typography>
                    <Typography>
                      Resolva nossos desafios e suba no ranking
                    </Typography>
                  </CardContent>
                </Link>
              </Card>
            </Grid>
          </Grid>
        </Container>
      </main>
      <Grid className={classes.grid} item xs={12}>
        <Footer />
      </Grid>
      {/* End footer */}
    </React.Fragment>
  );
}
