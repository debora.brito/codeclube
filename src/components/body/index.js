import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import "./styles.css";
import LogoCode from "../../imagens/logo.png";
import Scratch from "../../imagens/scratch.jpg";
import Microbit from "../../imagens/microbit.png";
import Atividade from "../../imagens/computacao.png";
import Midias from "../../imagens/midias.png";
import PET from "../../imagens/pet.png";
import Ufopa from "../../imagens/ufopa.png";
import Footer from "../footer/footer";

const useStyles = makeStyles((theme) => ({
  root: {
    marginRight: "100px",
    marginLeft: "100px",
    marginTop: "20px",
    textAlign: "center",
    display: "flex",
  },
  heroContent: {
    padding: theme.spacing(7, 1, 6),
  },
  dsbText: {
    marginTop: "0px",
    fontWeight: "bold",
    textAlign: "center",
  },
  title: {
    justifyContent: "center",
    display: "grid",
    fontFamily: "Lobster",
    letterSpacing: "2px",
    color: "#000088",
    paddingTop: "20px",
  },
  text: {
    color: "#0000ba",
    paddingTop: "20px",
    marginLeft: "150px",
    marginRight: "150px",
  },
  dsbBoxPage: {
    marginTop: "50px",
    backgroundColor: "#e3f2fd",
  },
}));

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  "@global": {
    ".MuiContainer-maxWidthLg": {
      maxWidth: "inherit",
    },
    ".MuiTypography-h5": {
      fontWeight: "500",
    },
  },
})(() => null);

export default function Title() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <GlobalCss />
      <div className={classes.heroContent}>
        <Grid container spacing={2} className="dsbBox">
          <Grid item xs zeroMinWidth>
            <Typography className="dsbTitle" component="div" variant="h5">
              Code Clube na Escola
            </Typography>
          </Grid>
          <Grid item xs={12} zeroMinWidth>
            <Typography
              classes={{
                root: classes.root,
              }}
              className="body"
              component="div"
              variant="body1"
            >
              Motivados pelo potencial da inserção da computação no auxílio ao
              desenvolvimento do pensamento computacional de crianças, jovens e
              adultos, o Laboratório da Universidade Federal do Oeste do Pará
              (UFOPA) desenvolve o programa “Mídias Eletrônicas: Ensino e
              Inclusão”, o laboratório realiza atividades extracurriculares para
              disseminar o ensino de programação para os estudantes na cidade de
              Santarém-PA, através da criação de clubes de programação
              (CodeClub). Nos clubes os instrutores ensinam por meio da oferta
              de cursos e oficinas, os alunos a programar criando jogos e
              animações para diferentes plataformas.
            </Typography>
          </Grid>
          <Grid item xs={12} zeroMinWidth>
            <img className="dsbImage" src={LogoCode} alt="Robos Code Clube" />
          </Grid>
          <Grid item xs={12} zeroMinWidth>
            <Typography
              classes={{
                body1: classes.dsbText,
              }}
              component="div"
              variant="body1"
              style={{
                paddingBottom: "20px",
              }}
            >
              A filosofia do Code Club é diversão, criatividade e aprendizagem
              pela descoberta!
            </Typography>
          </Grid>
        </Grid>
      </div>

      <Grid container spacing={1}>
        <Grid item xs >
          <Typography
            noWrap
            variant="h4"
            component="div"
            classes={{
              h4: classes.title,
            }}
          >
            Recursos
          </Typography>
        </Grid>

        <Grid container spacing={1}> 
        <Grid item xs style={{ justifyContent: 'center', display:'flex'}}>
        <figure >
            <img className="circle" src={Scratch} alt="scratch" />
            <figcaption>
              <Typography variant="h6"> Scratch</Typography>
            </figcaption>
          </figure>
        </Grid>
        <Grid item xs style={{ justifyContent: 'center', display:'flex'}}>
        <figure>
            <img className="circle" src={Microbit} alt="microbit" />
            <figcaption>
              <Typography variant="h6"> Microbit</Typography>
            </figcaption>
          </figure>
        </Grid>
        <Grid item xs style={{ justifyContent: 'center', display:'flex', paddingLeft: '55px'}}>
        <figure>
            <img
              className="circle"
              src={Atividade}
              alt="Atividades Desplugadas"
            />
            <figcaption>
              <Typography style={{marginLeft: '-20px'}} variant="h6"> Atividades Desplugadas</Typography>
            </figcaption>
          </figure>
      </Grid>
      </Grid>
      <Grid item xs >
          <Typography
            variant="body1"
            classes={{
              body1: classes.text,
            }}
            style={{textAlign: "center"}}
          >
            O Code Clube busca explorar o Pensamento Computacional de crianças e
            adolescentes através de técnicas educacionais, utilizando de
            recursos didáticos para aprender a programar, como o Scratch, uma
            plataforma de programação em blocos lúdica e educativa, o Microbit,
            uma pequena placa programavel criada para fins educacionais e a
            commputação desplugada, onde o aluno aprende conceitos
            computacionais sem precisar de um computador.
          </Typography>
          </Grid>
      </Grid>

      <Grid
        container
        spacing={3}
        classes={{
          container: classes.dsbBoxPage,
        }}
        style={{marginLeft: '0px'}}
        justify="center"
      >
        <Grid item xs={12} zeroMinWidth className="dsbText">
          <Typography variant="h4" style={{ fontFamily: "Lobster" }}>
            Parceiros
          </Typography>
        </Grid>
        <Grid item>
          <Grid container justify="center" >
            <img className="img" src={Midias} alt="logo midias" />
            <img className="img2" src={Ufopa} alt="logo ufopa" />
            <img className="img" src={PET} alt="logo pet" />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Footer />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
