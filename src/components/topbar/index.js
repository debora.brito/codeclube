import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import CssBaseline from "@material-ui/core/CssBaseline";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Slide from "@material-ui/core/Slide";
import { withStyles } from "@material-ui/core/styles";
import Cadastro from "../auth/cadastro.js";
import Login from "../auth/login";

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  "@global": {
    // You should target [class*="MuiButton-root"] instead if you nest themes.
    ".MuiAppBar-colorPrimary": {
      backgroundColor: "#000088",
      boxShadow: "0 2px 4px rgba(0, 0, 0, 0.5)",
    },
    ".MuiTypography-subtitle1": {
      padding: "8px 12px",
    },
    ".MuiTypography-h6": {
      flexGrow: "1",
    },
    ".MuiToolbar-regular": {
      minHeight: '0px'
    } 
  },
})(() => null);

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default function HideAppBar(props) {
  return (
    <div>
      <CssBaseline />
      <GlobalCss />
      <HideOnScroll {...props}>
        <AppBar position="fixed">
          <Toolbar variant="dense" style={{marginTop: '10px'}}>
            <Typography variant="h6">Code Clube</Typography>
            <Login />
            <Cadastro />
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <Toolbar />
    </div>
  );
}
