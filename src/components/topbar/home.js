import React, { Component } from "react";
import api from "../../services/api";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { withStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Morty from "../../imagens/Morty.jpg";
import TopBar from "./index";

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  "@global": {
    // You should target [class*="MuiButton-root"] instead if you nest themes.
    ".MuiAppBar-colorPrimary": {
      backgroundColor: "#000088",
      boxShadow: "0 2px 4px rgba(0, 0, 0, 0.5)",
    },
    ".MuiTypography-subtitle1": {
      padding: "8px 12px",
    },
    ".MuiTypography-h6": {
      flexGrow: "1",
    },
    // ".MuiButton-label": {
    //   color: "white",
    // },
    ".MuiSvgIcon-root": {
      width: "1.5em",
    },
  },
})(() => null);

export default class Home extends Component {
  state = {
    anchorEl: null,
    nome: "",
  };

  componentDidMount() {
    api.post("/details").then(
      (res) => {
        this.setState({
          user: res.data,
          nome: res.data.user.name,
        });
      },
      (err) => {
        console.log(err);
      }
    );
  }

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handlerLogout = () => {
    sessionStorage.clear();
    window.location.reload();
  };

  render() {
    if (this.state.user) {
      return (
        <div>
          <GlobalCss />
          <AppBar>
            <Toolbar style={{ justifyContent: "space-between" }}>
              <a href="/" style={{ color: "white" }}>
                <Typography variant="h6">Code Clube</Typography>
              </a>
              <Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={this.handleClick}
              >
                <font style={{ padding: "10px", color: "white" }}>
                  {" "}
                  {this.state.nome.split(" ")[0]}{" "}
                </font>
                <Avatar alt="Morty" src={Morty} />
              </Button>
              <Menu
                id="simple-menu"
                anchorEl={this.state.anchorEl}
                keepMounted
                open={Boolean(this.state.anchorEl)}
                onClose={this.handleClose}
              >
                <MenuItem onClick={this.handleClose}>Perfil</MenuItem>
                <MenuItem onClick={this.handleClose}>Configurações</MenuItem>
                <MenuItem onClick={this.handlerLogout}>Sair</MenuItem>
              </Menu>{" "}
            </Toolbar>
          </AppBar>
        </div>
      );
    }
    return <TopBar />;
  }
}
