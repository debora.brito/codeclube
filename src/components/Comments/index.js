import React, { Component } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import api from "../../services/api";
import { withRouter } from "react-router-dom";

class NestedList extends Component {
  state = {
    open: false,
    resposta: "",
    post_id: "",
    user: "",
    error: "",
  };

  handleClick = () => {
    this.setState({ open: !this.state.open });
  };

  handlePost = async (e) => {
    e.preventDefault();
    const { resposta, user_id, post_id } = this.state;
    if (!resposta) {
      this.setState({ error: "Preencha todos os dados para se cadastrar" });
    } else {
      try {
        await api.post("/comentarios", { resposta, user_id, post_id });
        window.location.reload();
      } catch (err) {
        console.log(err);
        this.setState({ error: "Ocorreu um erro ao registrar sua conta. T.T" });
      }
    }
  };

  handleClickOpen = () => {
    this.setState({ post_id: this.props.match.params.id });
  };

  render() {
    return (
      <List component="nav" aria-labelledby="nested-list-subheader">
        <Button
          variant="outlined"
          color="primary"
          disableElevation
          onClick={this.handleClick}
        >
          Responder
          {this.state.open ? <ExpandLess /> : <ExpandMore />}
        </Button>
        <form
          method="POST"
          onSubmit={this.handlePost}
          noValidate
          autoComplete="off"
        >
          <Collapse in={this.state.open} timeout="auto" unmountOnExit>
            <ListItem style={{ padding: "0" }}>
              <TextField
                label="Resposta"
                multiline
                rows={8}
                variant="outlined"
                fullWidth
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => this.setState({ resposta: e.target.value })}
                multiline
              />
            </ListItem>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              onClick={(e) =>
                this.setState({ post_id: this.props.match.params.id })
              }
            >
              Enviar
            </Button>
          </Collapse>
        </form>
      </List>
    );
  }
}

export default withRouter(NestedList);
