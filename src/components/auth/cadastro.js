import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import EmailIcon from "@material-ui/icons/Email";
import PersonIcon from "@material-ui/icons/Person";
import InputAdornment from "@material-ui/core/InputAdornment";
import LockIcon from "@material-ui/icons/Lock";
import InsertEmoticonIcon from "@material-ui/icons/InsertEmoticon";
import FormHelperText from "@material-ui/core/FormHelperText";
import MenuItem from "@material-ui/core/MenuItem"; // Para
import { withRouter } from "react-router-dom";

// ...
import api from "../../services/api";

class SignUp extends Component {
  state = {
    name: "",
    email: "",
    password: "",
    serie: "",
    error: "",
    open: false,
    fullWidth: true,
    maxWidth: "xs",
    currency: "",
    sucess: "",
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = (event) => {
    this.setState({ currency: event.target.value });
  };

  teste = (event) => {
    this.setState({ serie: event.target.value });
  };

  twoCalls = (e) => {
    this.teste(e);
    this.handleChange(e);
  };

  handleSignUp = async (e) => {
    e.preventDefault();
    const { name, serie, email, password } = this.state;
    if (!name || !serie || !email || !password) {
      this.setState({ error: "Preencha todos os dados para se cadastrar" });
    } else {
      try {
        await api.post("/alunos", { name, serie, email, password });
        this.setState({
          sucess:
            "Cadastro realizado com sucesso, tenha acesso a página ao fazer o login",
        });
        this.props.history.push("/");
      } catch (err) {
        console.log(err);
        this.setState({ error: "Ocorreu um erro ao registrar sua conta. T.T" });
      }
    }
  };

  render() {
    return (
      <div>
        <Button color="inherit" onClick={this.handleClickOpen}>
          Cadastre-se
        </Button>

        <Dialog
          fullWidth={this.state.fullWidth}
          maxWidth={this.state.maxWidth}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <form
            method="POST"
            onSubmit={this.handleSignUp}
            noValidate
            autoComplete="off"
          >
            <DialogTitle style={{ color: "#000088" }} id="form-dialog-title">
              Faça seu cadastro!
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                Para se cadastrar na plataforma, por favor insira suas
                informações aqui.
              </DialogContentText>
              <FormHelperText style={{ color: "red" }}>
                {this.state.error && <span>{this.state.error}</span>}
              </FormHelperText>
              <FormHelperText style={{ color: "green" }}>
                {this.state.sucess && (
                  <span>
                    {" "}
                    <InsertEmoticonIcon />
                    {this.state.sucess}
                  </span>
                )}
              </FormHelperText>
              <TextField
                autoFocus
                margin="dense"
                id="filled-basic"
                variant="filled"
                label="Nome Completo"
                type="text"
                required
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <PersonIcon color="disabled" />
                    </InputAdornment>
                  ),
                }}
                onChange={(e) => this.setState({ name: e.target.value })}
              />
              <TextField
                autoFocus
                margin="dense"
                id="filled-basic"
                variant="filled"
                label="Endereço de e-mail"
                type="email"
                required
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <EmailIcon color="disabled" />
                    </InputAdornment>
                  ),
                }}
                onChange={(e) => this.setState({ email: e.target.value })}
              />
              <TextField
                select
                id="demo-simple-select-filled"
                label="Série"
                variant="filled"
                value={this.state.currency}
                onChange={this.twoCalls}
                fullWidth
              >
                <MenuItem value={8}>8º ano</MenuItem>
                <MenuItem value={9}>9º ano</MenuItem>
                <MenuItem value={1}>1º ano</MenuItem>
                <MenuItem value={2}>2º ano</MenuItem>
              </TextField>
              <TextField
                autoFocus
                margin="dense"
                id="filled-basic"
                variant="filled"
                label="Senha"
                type="text"
                required
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <LockIcon color="disabled" />
                    </InputAdornment>
                  ),
                }}
                onChange={(e) => this.setState({ password: e.target.value })}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Cancelar
              </Button>
              <Button type="submit" color="primary">
                Concluir
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}
export default withRouter(SignUp);
