import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import EmailIcon from "@material-ui/icons/Email";
import InputAdornment from "@material-ui/core/InputAdornment";
import Link from "@material-ui/core/Link";
import LockIcon from "@material-ui/icons/Lock";
import api from "../../services/api";
import { login } from "../../services/auth";
import { withRouter } from "react-router-dom";

class SignIn extends Component {
  state = {
    email: "",
    password: "",
    error: "",
    open: false,
    fullWidth: true,
    maxWidth: "xs",
    sucess: "",
    values: {
      amount: "",
      password: "",
      weight: "",
      weightRange: "",
      showPassword: false,
    },
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleSignIn = async (e) => {
    e.preventDefault();
    const { email, password } = this.state;
    if (!email || !password) {
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
    } else {
      try {
        const response = await api.post("/login", {
          email,
          password,
        });
        login(response.data.access_token);
        window.location.reload('/home')
        
      } catch (err) {
        this.setState({
          error:
            "Houve um problema com o login, verifique suas credenciais. T.T",
        });
      }
    }
  };


  render() {
    return (
      <div>
        <Button color="inherit" variant="text" onClick={this.handleClickOpen}>
          Login
        </Button>

        <Dialog
          fullWidth={this.state.fullWidth}
          maxWidth={this.state.maxWidth}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <form
            method="POST"
            onSubmit={this.handleSignIn}
            noValidate
            autoComplete="off"
          >
            {this.state.error && <p>{this.state.error}</p>}
            <DialogTitle style={{ color: "#000088" }} id="form-dialog-title">
              Entre na sua conta!
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                Para entrar na plataforma, por favor preencha os campos abaixo.
              </DialogContentText>
              <TextField
                autoFocus
                margin="dense"
                id="filled-basic"
                variant="filled"
                label="Endereço de e-mail"
                type="email"
                onChange={(e) => this.setState({ email: e.target.value })}
                required
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <EmailIcon color="disabled" />
                    </InputAdornment>
                  ),
                }}
              />
              <TextField
                autoFocus
                margin="dense"
                id="filled-basic"
                variant="filled"
                label="Senha"
                type="password"
                required
                fullWidth
                onChange={(e) => this.setState({ password: e.target.value })}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <LockIcon color="disabled" />
                    </InputAdornment>
                  ),
                }}
              />
              <Link color="primary" href="/">
                Esqueceu sua senha?
              </Link>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Cancelar
              </Button>
              <Button type="submit" color="primary">
                Entrar
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}

export default withRouter(SignIn);
