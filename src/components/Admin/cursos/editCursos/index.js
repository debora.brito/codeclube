import Grid from "@material-ui/core/Grid";
import React, { Component } from "react";
import api from "../../../../services/api";
import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import FormHelperText from "@material-ui/core/FormHelperText";
import TextField from "@material-ui/core/TextField";
import { getCourse } from "../lista/index";

class EditCurso extends Component {
	state = {
		open: false,
		nome_curso: "",
		descricao: "",
		thumbnail: "",
		video: "",
		error: "",
	};

	handleClickOpen = () => {
		this.setState({ open: true });
	};

	handleClose = () => {
		this.setState({ open: false });
	};

	// useEffect(() => {
	//     const fetchPosts = async () => {
	//       const res = await api.get("/posts");
	//       setPosts(res.data.posts);
	//     };
	//     fetchPosts();
	//   }, []);

	// componentDidMount() {
	// 	const id = this.props.match.params.id;
	// 	api.get('/cursos').then(
	// 		(res) => {
	// 			this.setState({
	// 				nome_curso: res.data.post.nome_curso,
	// 				descricao: res.data.post.descricao,
	// 				thumbnail: res.data.post.thumbnail,
	// 			});
	// 			console.log(res);
	// 		},
	// 		(err) => {
	// 			console.log(err);
	// 		}
	// 	);
	// }

	//   handleUpdate = async (e) => {
	//     e.preventDefault();
	//     const { nome_curso, descricao, thumbnail} = this.state;
	//     const id = this.props.match.params.id
	//     if (!nome_curso || !descricao || !thumbnail) {
	//       this.setState({ error: "Preencha todos os dados para se cadastrar" });
	//     } else {
	//       try {
	//         await api.put(`/posts/${id}`, { nome_curso, descricao, thumbnail });
	//         window.location.reload()
	//       } catch (err) {
	//         console.log(err);
	//         this.setState({ error: "Ocorreu um erro ao registrar sua conta. T.T" });
	//       }
	//     }
    //   }
    
		

	render() {
		return (
			<Grid item>
				<IconButton
					variant="outlined"
					color="primary"
					onClick={this.handleClickOpen}
				>
					<EditIcon />
				</IconButton>
				<Dialog
					open={this.state.open}
					onClose={this.handleClose}
					aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
				>
					<form
						method="PUT"
						onSubmit={this.handleUpdate}
						noValidate
						autoComplete="off"
					>
						<DialogTitle id="form-dialog-title">
							Edite o curso
						</DialogTitle>
						<DialogContent>
							<DialogContentText>
								Para editar o curso preencha os campos abaixo.
							</DialogContentText>
							<FormHelperText style={{ color: "red" }}>
								{this.state.error && (
									<span>{this.state.error}</span>
								)}
							</FormHelperText>
							<TextField
								autoFocus
								margin="dense"
								id="standard-textarea"
								label="nome_curso"
								defaultValue={this.state.nome_curso}
								fullWidth
								variant="filled"
								multiline
								InputLabelProps={{
									shrink: true,
								}}
								onChange={(e) =>
									this.setState({
										nome_curso: e.target.value,
									})
								}
							/>
							<TextField
								autoFocus
								margin="dense"
								id="standard-textarea"
								label="Descrição"
								defaultValue={this.state.descricao}
								multiline
								fullWidth
								variant="filled"
								rows={3}
								InputLabelProps={{
									shrink: true,
								}}
								onChange={(e) =>
									this.setState({ descricao: e.target.value })
								}
							/>
							<TextField
								autoFocus
								margin="dense"
								id="standard-textarea"
								label="Video"
								defaultValue={this.state.thumbnail}
								multiline
								fullWidth
								variant="filled"
								InputLabelProps={{
									shrink: true,
								}}
								onChange={(e) =>
									this.setState({ thumbnail: e.target.value })
								}
							/>
						</DialogContent>
						<DialogActions>
							<Button onClick={this.handleClose} color="primary">
								Cancelar
							</Button>
							<Button type="submit" color="primary">
								Enviar
							</Button>
						</DialogActions>
					</form>
				</Dialog>
			</Grid>
		);
	}
}

export default withRouter(EditCurso);
