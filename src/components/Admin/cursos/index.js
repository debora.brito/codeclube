import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import InsertEmoticonIcon from "@material-ui/icons/InsertEmoticon";
import FormHelperText from "@material-ui/core/FormHelperText";
import api from "../../../services/api";
import "./styles.css";

class CadastrarCurso extends Component {
	state = {
		thumbnail: null,
		nome_curso: "",
		video: "",
		descricao: "",
		error: "",
		open: false,
	};

	handleClickOpen = () => {
		this.setState({ open: true });
	};

	handleClose = () => {
		this.setState({ open: false });
	};

	onFileChange = (event) => {
		this.setState({ thumbnail: event.target.files[0] });
	};

	onFileUpload = () => {
		if (
			!this.state.nome_curso ||
			!this.state.video ||
			!this.state.thumbnail ||
			!this.state.descricao
		) {
			this.setState({
				error: "Preencha todos os campos para cadastrar o curso",
			});
		} else {
			const formData = new FormData();
			formData.append(
				"thumbnail",
				this.state.thumbnail,
				this.state.thumbnail.name
			);
			formData.append("nome_curso", this.state.nome_curso);
			formData.append("video", this.state.video);
			formData.append("descricao", this.state.descricao);
			api.post("/cursos", formData, {
				headers: {
					"Content-Type": "multipart/form-data",
				},
			});
			this.setState({
				sucess: "Curso Cadastrado com Sucesso!",
			});
		}
	};

	render() {
		return (
			<div className="root icon">
				<Button
					variant="contained"
					color="primary"
					onClick={this.handleClickOpen}
				>
					Cadastrar um Novo Curso
				</Button>

				<Dialog
					open={this.state.open}
					onClose={this.handleClose}
					aria-labelledby="form-dialog-title"
				>
					<DialogTitle id="form-dialog-title">
						Criar Novo Curso
					</DialogTitle>
					<DialogContent>
						<DialogContentText>
							Preencha os campos para criar um novo curso
						</DialogContentText>
						<FormHelperText style={{ color: "red" }}>
							{this.state.error && (
								<span>{this.state.error}</span>
							)}
						</FormHelperText>
						<FormHelperText style={{ color: "green" }}>
							{this.state.sucess && (
								<span>
									{" "}
									<InsertEmoticonIcon />
									{this.state.sucess}
								</span>
							)}
						</FormHelperText>
						<TextField
							autoFocus
							margin="dense"
							id="nome_curso"
							label="Nome do Curso"
							placeholder="Insira o nome do curso"
							type="text"
							fullWidth
							variant="filled"
							onChange={(e) =>
								this.setState({
									nome_curso: e.target.value,
								})
							}
						/>
						<TextField
							autoFocus
							margin="dense"
							id="descricao"
							type="text"
							autoFocus
							margin="dense"
							label="Descrição"
							placeholder="Faça uma breve descrição do assunto"
							multiline
							fullWidth
							variant="filled"
							rows={5}
							onChange={(e) =>
								this.setState({ descricao: e.target.value })
							}
						/>
						<TextField
							autoFocus
							margin="dense"
							type="url"
							autoFocus
							margin="dense"
							label="URL de vídeo"
							placeholder="Insira aqui a URL do seu video"
							multiline
							fullWidth
							variant="filled"
							onChange={(e) =>
								this.setState({ video: e.target.value })
							}
						/>
						<input
							accept="image/*"
							className="input"
							id="contained-button-file"
							multiple
							type="file"
							onChange={this.onFileChange}
						/>
						<div>
							<label htmlFor="contained-button-file">
								<Button
									variant="contained"
									color="primary"
									component="span"
								>
									Upload Capa do Curso
								</Button>
							</label>
						</div>
					</DialogContent>
					<DialogActions>
						<Button onClick={this.handleClose} color="primary">
							Cancelar
						</Button>
						<Button
							onClick={this.onFileUpload}
							type="submit"
							color="primary"
						>
							Confirmar
						</Button>
					</DialogActions>
				</Dialog>
			</div>
		);
	}
}

export default CadastrarCurso;
