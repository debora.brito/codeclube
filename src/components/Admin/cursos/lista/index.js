import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import FormHelperText from "@material-ui/core/FormHelperText";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import EditIcon from "@material-ui/icons/Edit";
import EditCurso from "../editCursos/index";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import api from "../../../../services/api";

const useStyles = makeStyles((theme) => ({
	root: {
		width: "90%",
		position: "absolute",
		left: "5%",
		top: "200px",
	},
}));

export default function SimpleAccordion() {
	const classes = useStyles();
	const [cursos, setCursos] = useState([]);
	const [open, setOpen] = useState(false);
	const initialValues = useState({
		nome_curso: "",
		descricao: "",
		thumbnail: "",
		video: "",
	});
	const [idCourse, setIdCourse] = useState("");

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useEffect(() => {
		const fetchCourses = async () => {
			const res = await api.get("/cursos");
			setCursos(res.data.cursos);
		};
		fetchCourses();
	}, []);


	const deleteCourse = (id) => {
		api.delete("/cursos/" + id).then((result) => {
			window.location.reload();
		});
	};

	return (
		<div className={classes.root}>
			{cursos.map((curso) => (
				<Accordion key={curso.id}>
					<AccordionSummary
						expandIcon={<ExpandMoreIcon />}
						aria-controls="panel1a-content"
						id="panel1a-header"
					>
						<Typography variant="h6">{curso.nome_curso}</Typography>
					</AccordionSummary>
					<AccordionDetails style={{ display: "grid" }}>
						<Typography>{curso.descricao}</Typography>
					</AccordionDetails>
					<AccordionDetails>
						<img
							style={{
								width: 200,
								height: 200,
								resizeMode: "contain",
							}}
							src={`http://localhost/code/storage/thumbnail/${curso.thumbnail}`}
						/>
					</AccordionDetails>
					<AccordionDetails>
						<Grid item>
							<IconButton
								variant="outlined"
								color="primary"
								onClick={handleClickOpen}
							>
								<EditIcon />
							</IconButton>
							<Dialog
								open={open}
								onClose={handleClose}
								aria-labelledby="alert-dialog-title"
								aria-describedby="alert-dialog-description"
							>
								<form
									method="PUT"
									// onSubmit={handleUpdate}
									noValidate
									autoComplete="off"
								>
									<DialogTitle id="form-dialog-title">
										Edite o curso
									</DialogTitle>
									<DialogContent>
										<DialogContentText>
											Para editar o curso preencha os
											campos abaixo.
										</DialogContentText>
										{/* <FormHelperText
											style={{ color: "red" }}
										>
											{error && (
												<span>{error}</span>
											)}
										</FormHelperText> */}
										<TextField
											autoFocus
											margin="dense"
											id="standard-textarea"
											label="nome_curso"
											// defaultValue={nome_curso}
											fullWidth
											variant="filled"
											multiline
											InputLabelProps={{
												shrink: true,
											}}
											// onChange={(e) =>
											// 	this.setState({
											// 		nome_curso: e.target.value,
											// 	})
											// }
										/>
										<TextField
											autoFocus
											margin="dense"
											id="standard-textarea"
											label="Descrição"
											// defaultValue={descricao}
											multiline
											fullWidth
											variant="filled"
											rows={3}
											InputLabelProps={{
												shrink: true,
											}}
											// onChange={(e) =>
											// 	this.setState({
											// 		descricao: e.target.value,
											// 	})
											// }
										/>
										<TextField
											autoFocus
											margin="dense"
											id="standard-textarea"
											label="Video"
											// defaultValue={thumbnail}
											multiline
											fullWidth
											variant="filled"
											InputLabelProps={{
												shrink: true,
											}}
											// onChange={(e) =>
											// 	this.setState({
											// 		thumbnail: e.target.value,
											// 	})
											// }
										/>
									</DialogContent>
									<DialogActions>
										<Button
											onClick={handleClose}
											color="primary"
										>
											Cancelar
										</Button>
										<Button type="submit" color="primary">
											Enviar
										</Button>
									</DialogActions>
								</form>
							</Dialog>
						</Grid>
						<IconButton
							variant="outlined"
							color="secondary"
                            onClick={() => {
                                deleteCourse(curso.id);
                              }}
						>
							<DeleteIcon />
						</IconButton>
					</AccordionDetails>
				</Accordion>
			))}
		</div>
	);
}
