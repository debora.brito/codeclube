import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";

const useStyles = makeStyles((theme) => ({
  root: {
    marginRight: "100px",
    marginLeft: "100px",
    marginTop: "20px",
    textAlign: "center",
    display: "flex",
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(6),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
}));

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Code Clube
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  "@global": {
    ".MuiContainer-maxWidthLg": {
      maxWidth: "inherit",
    },
    ".MuiTypography-h5": {
      fontWeight: "500",
    },
  },
})(() => null);

export default function Title() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <GlobalCss />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Container
            maxWidth="md"
            component="footer"
            className={classes.footer}
          >
            <Typography className="dsbBody" variant="body1" style={{textAlign: 'center'}}>
              Nossa missão é levar a educação, informação e a tecnologia para
              todos, em qualquer lugar e sem fins lucrativos.
            </Typography>
            <Box mt={3}>
              <Copyright />
            </Box>
          </Container>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
