import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { PaginationItem } from "@material-ui/lab";
import { withStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginTop: theme.spacing(2),
    },
    justifyContent: 'center',
    display: 'flex',
    alignItems: 'center',
    color: 'blue'
  },
  '.MuiPaginationItem-page':{
    "&:hover": {
      backgroundColor: 'rgba(0, 0, 186, 1)'
    },
}
}));

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  "@global": {
    '.MuiPaginationItem-page':{
      "&:hover": {
        backgroundColor: 'rgba(0, 0, 186, 0.2)'
      },
  },
  '.MuiPaginationItem-outlined': {
    border: '1px solid rgba(0, 0, 186, 1)',
}
  },
})(() => null);


export default function PaginationSize({ postsPerPage, totalPosts, paginate }) {
  const classes = useStyles();
  const pageNumber = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumber.push(i);
  }

  return (
    <div className={classes.root}>
      <GlobalCss />
      {pageNumber.map((number) => (
        <PaginationItem key={number}
          color="primary"
          variant="outlined"
          count={number}
          onClick={() => paginate(number)}
          page={number}
        />
      ))}
    </div>
  );
}
