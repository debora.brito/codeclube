import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import index from "../components/body/index";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";
import Body from"../components/body/home"
import Forum from "../components/forum/home"
import Show from "../components/forum/show"
import Cursos from "../components/Cursos/index"

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <PublicRoute exact path="/" component={index} />
      <PrivateRoute path="/home" component={Body} />
      <PrivateRoute exact path="/forum" component={Forum} />
      <PrivateRoute exact path="/forum/post/:id" component={props => <Show {...props} /> } />
      <PrivateRoute exact path="/cursos" component={Cursos} />
      <Route path="*" component={() => <h1>Page not found</h1>} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
