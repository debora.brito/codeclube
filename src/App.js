import React from "react";

import "./styles.css";
import Routes from "./routes/routes";
import TopBar from "./components/topbar/home"

const App = () => (
  
  <div className="App">
    <Routes />
    <TopBar />
  </div>
);

export default App;
